#!/usr/bin/bash
echo "Script needs Ookla's speedtest binary in this container's volume (named wireguard-speedtest)"
echo "Script requires Mullvad config files, wireguard, docker and jq"
echo "Change VPN configs directory and results output directory as needed in $BASH_SOURCE"

configdir="/var/lib/docker/volumes/wireguard-speedtest/_data/mullvad_wireguard/"
outputdir="/mnt/Media" # without slash at the end

# 14-eyes countries
find $configdir -type f \( -iname "*-us*" \
    -o -iname "*-gb*" \
    -o -iname "*-ca*" \
    -o -iname "*-nz*" \
    -o -iname "*-au*" \
    -o -iname "*-dk*" \
    -o -iname "*-fr*" \
    -o -iname "*-nl*" \
    -o -iname "*-no*" \
    -o -iname "*-de*" \
    -o -iname "*-at*" \ # 15th eye?
    -o -iname "*-be*" \
    -o -iname "*-it*" \
    -o -iname "*-se*" \
    -o -iname "*-es*" \) \
    -delete \
    -exec echo "Deleting {} - 14-eyes country" \;

# Not in EU
find $configdir -type f \( -iname "*-br*" \
    -o -iname "*-hk*" \
    -o -iname "*-il*" \
    -o -iname "*-jp*" \
    -o -iname "*-sg*" \
    -o -iname "*-ae*" \) \
    -delete \
    -exec echo "Deleting {} - Not in EU" \;

docker run -d \
    --name=wireguard-speedtest \
    --cap-add=NET_ADMIN \
    --cap-add=SYS_MODULE \
    -e PUID=1000 \
    -e PGID=1000 \
    -e TZ=Europe/Vilnius \
    -p 51821:51820/udp \
    -v wireguard-speedtest:/config \
    -v /lib/modules:/lib/modules \
    --sysctl="net.ipv4.conf.all.src_valid_mark=1" \
    --sysctl="net.ipv6.conf.all.disable_ipv6=0" \
    lscr.io/linuxserver/wireguard >/dev/null
sleep 1

echo -e "\n Running the speedtests."
echo > /tmp/speedtest.csv
for file in $(find $configdir -type f -name "*.conf" | sort);do
    docker exec -it wireguard-speedtest /bin/bash wg-quick down /config/wg0.conf >/dev/null
    rm /var/lib/docker/volumes/wireguard-speedtest/_data/wg0.conf
    cp $file /var/lib/docker/volumes/wireguard-speedtest/_data/wg0.conf
    docker exec -it wireguard-speedtest /bin/bash wg-quick up /config/wg0.conf >/dev/null
    results=$(docker exec wireguard-speedtest /config/speedtest --accept-license --accept-gdpr -u Mbps -p no | grep -E "Download|Upload" | awk '{print $3}')
    download=$(echo $results | awk '{print $1}')
    upload=$(echo $results | awk '{print $2}')
    ip=$(docker exec -it wireguard-speedtest curl -s ipinfo.io | jq '.ip' | sed -E 's/\"//g')
    filename=$(basename $file)
    echo "$filename,$download Mb/s,$upload Mb/s,$ip"
    echo "$filename,$download Mb/s,$upload Mb/s,$ip" >> /tmp/speedtest.csv
done

echo "Removing wireguard container"
docker container stop wireguard-speedtest;docker container rm wireguard-speedtest

# Output sorted by upload speed
echo -e "\nCreating output file $outputdir/speedtest.csv"
echo "Config,Download,Upload,IP" > $outputdir/speedtest.csv
cat /tmp/speedtest.csv | sort -t, -k+3 -r -n >> $outputdir/speedtest.csv
rm /tmp/speedtest.csv
